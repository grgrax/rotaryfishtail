-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_rotaryfishtail_new
CREATE DATABASE IF NOT EXISTS `db_rotaryfishtail_new` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_rotaryfishtail_new`;


-- Dumping structure for table db_rotaryfishtail_new.tbl_articles
CREATE TABLE IF NOT EXISTS `tbl_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `order` tinyint(3) unsigned DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `image_title_2` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `video_title` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `embed_code` varchar(10000) DEFAULT NULL,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `meta_key` varchar(100) DEFAULT NULL,
  `meta_desc` varchar(10000) DEFAULT NULL,
  `meta_robots` varchar(100) DEFAULT NULL,
  `url1` varchar(100) DEFAULT NULL,
  `url2` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_articles_ibfk_1` (`category_id`),
  KEY `tbl_articles_ibfk_3` (`author`),
  KEY `tbl_articles_ibfk_4` (`modified_by`),
  CONSTRAINT `FK_tbl_articles_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`),
  CONSTRAINT `FK_tbl_articles_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_articles_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_articles: ~114 rows (approximately)
/*!40000 ALTER TABLE `tbl_articles` DISABLE KEYS */;
INSERT INTO `tbl_articles` (`id`, `category_id`, `name`, `title`, `slug`, `content`, `status`, `order`, `image`, `image_title`, `image_title_2`, `video`, `video_title`, `video_url`, `embed_code`, `author`, `modified_by`, `meta_key`, `meta_desc`, `meta_robots`, `url1`, `url2`, `created_at`, `updated_at`) VALUES
	(51, 4, 'how it works', NULL, 'how-it-works', '<ol><li><b><h2>step 1</h2></b><p>ok man</p></li><li><b><h2>step 2</h2></b><p>yes man</p></li><li><b><h2>step 2</h2></b><p>be ready</p></li></ol>', 1, NULL, '524440_4362996394967_145472913_n.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:21:41', '0000-00-00 00:00:00'),
	(52, 38, 'How can i create Campaign here', NULL, 'how-can-i-create-campaign-here', '<p>Just sign up and your campaign is ready</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:15'),
	(53, 4, 'about us', NULL, 'about-us', '<p>Rotary International is a voluntary organization of business and professional leaders which provide humanitarian servicesone to help to build goodwill and peace in the world. There are more than 1.2 million (12 Lakhs) members 34000 Rotary clubs in over 200 countries and geographical areas. [Rotary\'s top priority is the global graphical eradication of polio.] Founded in Chicago in 1905, The Rotary foundation has awarded more than U.S. $ 2.1 billion in grants, which are administered at the local level by Rotary Clubs.</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:43', '2015-09-01 19:21:24'),
	(54, 4, 'useful links', NULL, 'useful-links', '<ul><li><a target="_blank" rel="nofollow" href="http://www.google.com">List items 1</a></li><li>List items 2</li><li>List items 3</li><li>list items 4</li></ul><br><br>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:23:12', '2015-07-25 17:59:03'),
	(57, 38, 'What is Ourlibrary.com.au', NULL, 'what-is-ourlibrarycomau', 'Ourlibrary.com.au is fundraising website&nbsp;', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:12'),
	(60, 38, 'How i will receive raised donation once campaign get completed?', NULL, 'how-i-will-receive-raised-donation-once-campaign-get-completed', '<p>You will recieve your raised donation through your PAYPAL or BANK ACCOUNT</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:31:28', NULL),
	(61, 38, 'Alexis Mercer', NULL, 'alexis-mercer', '<p>dfdfdf</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:45:32', NULL),
	(62, 38, 'Devin Hartman', NULL, 'devin-hartman', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto nihil saepe laborum architecto, consequuntur mollitia eius quas at cumque rem, nobis reiciendis doloribus. Blanditiis dolorem temporibus suscipit tempora nostrum saepe.</p><div><br></div>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 21:45:07', '2015-07-25 18:01:10'),
	(63, 38, 'Kimberly Nunez', NULL, 'kimberly-nunez', '<p>cc</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:20:47', NULL),
	(64, 38, 'Cain Ball', NULL, 'cain-ball', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:13', NULL),
	(65, 38, 'Kadeem Rios', NULL, 'kadeem-rios', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:39', NULL),
	(66, 38, 'Brenna Barron', NULL, 'brenna-barron', '<p>dfdfd</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:24:02', NULL),
	(112, 39, 'bye', 'm', 'm', '<p>ce</p>', 3, NULL, '0', 'pe', 'we', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 21:25:15', NULL),
	(113, 39, 'Micath Cobb', 'bytwo', 'micah-cobb', '<p>cc</p>', 1, NULL, '0', 'pp', 'ww', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:20:52', NULL),
	(114, 39, 'Kyle Delgado', 'bythree', 'kyle-delgado', '<p>cc</p>', 3, NULL, '0', 'ppp', 'www', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:21:59', NULL),
	(115, 39, 't', 'b', 't', '<p><i>?c</i><br></p>', 1, NULL, 'rtn-rama.jpg', 'p', 'w', NULL, NULL, NULL, NULL, 166, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 00:26:10', NULL),
	(116, 39, 'Denise Dean', 'Delectus ea proident maiores sed aut nisi illo ullam molestiae', 'denise-dean', '<p>c</p>', 3, NULL, '', 'Consequatur non dolore numquam voluptatibus ducimus beatae numquam eiusmod eu impedit nobis rem', 'Quos accusamus cillum sed tempore natus ex consequatur quos sed exercitationem reprehenderit maxime ', NULL, NULL, NULL, NULL, 157, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:11:03', NULL),
	(117, 39, 'a', 'Fugiat aut autem saepe enim repudiandae aliqua Neque nisi accusamus laborum inventore ipsam incididu', 'a', '<p>CDFDF</p>', 3, NULL, '', 'Error odit aliqua Quidem consequuntur sed fuga Commodi quas', 'Sint nihil occaecat perspiciatis ex', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:20:24', NULL),
	(118, 39, 'b', 'mm', 'mm', '<p>cc</p>', 3, NULL, '0', 'pe', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:40:09', NULL),
	(119, 39, 'Velit alias facilis sequi proident et nisi incididunt dolore et nostrum qui qui ab cillum ut', 'Eve Spencer', 'eve-spencer', '<p>fdfdfdeee</p>', 3, NULL, '', 'Architecto deserunt qui natus rerum fugiat id nihil', 'Blanditiis tempore pariatur Numquam consectetur voluptas et ad Nam porro suscipit sunt alias iure ea', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 09:14:32', NULL),
	(127, 40, 'p1', 'aa', 'aa', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 3, NULL, 'favico_24.png', 'aaa', 'aa', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 06:44:53', NULL),
	(136, 40, 'we', NULL, 'we', '<p>sd<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:12:38', NULL),
	(137, 40, 'tdfdsf', NULL, 'tdfdsf', '<p>dsfsdfdsf<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:14:16', NULL),
	(140, 40, 'tt', NULL, 'tt', '<p>ttt<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:28:55', NULL),
	(141, 40, 'heyge', NULL, 'hey', '<p>tttee<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL),
	(144, 41, 'none', NULL, 'none', '<p>tttee<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL),
	(145, 39, 'ne', 'b', 'ne', '<p>a?</p>', 3, NULL, '', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 18:07:28', NULL),
	(146, 40, 'Blair', NULL, 'blair-torres', '<p>dfdfdfee</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 18:07:59', NULL),
	(147, 40, 'Rama Foster', NULL, 'rama-foster', '<p>cceeE</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-25 19:35:07', NULL),
	(148, 40, 'oe', NULL, 'pone', '<p>c</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 20:09:23', NULL),
	(149, 40, 'ptwoe', NULL, 'ptwo', '<p>cedfdfdfdf</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 20:09:36', NULL),
	(151, 39, 'Rama Fostere', 'b', 'rama-fostere', '<p>c<br></p>', 3, NULL, '9781405304504L_001.jpg', 'o', 'wess', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 08:30:17', NULL),
	(152, 41, 'tn', NULL, 'tn', '<p>cc<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:06:56', NULL),
	(153, 41, 'fdsfj', NULL, 'fdsfj', '<p>dfdfdf<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:09:21', NULL),
	(154, 41, 'nc', NULL, 'nc', '<p><i>ccc</i><br></p><br><br>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:15:05', NULL),
	(155, 41, 'DLSJFLKJ', NULL, 'dlsjflkj', '<p>DJFKDF<br></p>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:17:50', NULL),
	(156, 41, 'dfdsjf', NULL, 'dfdsjf', '<p>dljfldk<br></p>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:19:43', NULL),
	(157, 41, 'tfdfdf', NULL, 'tfdfdf', '<p>t<br></p>', 3, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:22:00', NULL),
	(158, 41, 'adfjdskeh', NULL, 'dfjdsk', 'a<p>djfkde<br></p>', 3, NULL, '6a00e553bd675c88340148c6693d12970c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:03:18', NULL),
	(159, 39, 'fjdsfdhfdsf', 'dfdf', 'fjdsfdhfdsf', '<p>dlfjdsk<br></p>', 3, NULL, '0', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:08:55', NULL),
	(160, 39, 'dfdf', 'fjdsfdhfdsfdfd', 'fjdsfdhfdsfdfd', '<p>dlfjdsk<br></p>', 3, NULL, 'lion-riding_1739860i.jpg', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:09:54', NULL),
	(161, 39, 'GARY C.K. HUANG', 'MESSAGE FROM ROTARY INTERNATIONAL PRESIDENT', 'message-from-rotary-international-president', '<h5>Dear President Rtn. Rama Kanta Baral</h5><p>Congratulations on your assuming the office of President of your club for the year 2014-15. I also congratulate your dedicated team of club officers for a very successful year ahead</p><p><b>Light up Rotary</b>&nbsp;is the annual theme give by Rotary International President Gary C.K. Huang. who has challenged us to take action and make Rotary strong. One of the many ways to&nbsp;<b>Light Up Rotary</b>is to plan and host Rotary Day as an event to make new friends, exchange ideas, and take action to improve their local community and the world. Let us do it earnestly by highlighting the work of ordinary Rotary members doing extraordinary humanitarian work. Let us take this challenge together and Light Up Rotary.</p><p>All we asay and do must satisfy our own conscience as a Rotary Leade whether it benefits our clubs and our communities by adding some value is someone\'s life, which would ultimately satisfy our instinct of doing something good in the world. By satisfying our instinct, we can get joy at work and start enjoying Rotary.</p><p>Enjoy Rotary, by all means.</p><p>Yours-In-Rotary?</p>', 1, NULL, 'rtn-gary.jpg', 'RI President', '2014-15', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:48:16', NULL),
	(162, 39, 'RABINDRA PIYA', 'MESSAGE FROM DISTRICT GOVERNER', 'message-from-district-governer', '<h5>Dear President Rtn. Rama Kanta Baral</h5><p>Congratulations on your assuming the office of President of your club for the year 2014-15. I also congratulate your dedicated team of club officers for a very successful year ahead</p><p><b>Light up Rotary</b>&nbsp;is the annual theme give by Rotary International President Gary C.K. Huang. who has challenged us to take action and make Rotary strong. One of the many ways to&nbsp;<b>Light Up Rotary</b>is to plan and host Rotary Day as an event to make new friends, exchange ideas, and take action to improve their local community and the world. Let us do it earnestly by highlighting the work of ordinary Rotary members doing extraordinary humanitarian work. Let us take this challenge together and Light Up Rotary.</p><p>All we asay and do must satisfy our own conscience as a Rotary Leade whether it benefits our clubs and our communities by adding some value is someone\'s life, which would ultimately satisfy our instinct of doing something good in the world. By satisfying our instinct, we can get joy at work and start enjoying Rotary.</p><p>Enjoy Rotary, by all means.</p><p>Yours-In-Rotary?</p>', 1, NULL, 'rtn-rabindra.jpg', 'District Governor', '2013-14', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:49:38', NULL),
	(163, 39, 'RAMA KANTA BARAL', 'MESSAGE FROM PRESIDENT', 'message-from-president', '<h5>Dear Fellow Rotarians </h5><p>While assuming the office of the President of our prestigious club for 2014/15, at the very out set, I would like to extend my deep sense of gratitude to all my fellow rotarians for giving me this opportunity to act whole heartedly in giving direction and drive to the plans, aims and objectives of the club.</p><p>Working hand in hand with members of the club associated with different professions and business sectors, I feel somewhat elated to think, though tough, I will be working effectively to forward the humanatarian service in the true spirit of our commitment "Service above self". I feel pride to recount here a host of noble activities successfully conducted by this club since its inception in many areas of human concern such as education, health, poverty reduction, peace and family values and so on.</p><p>In the changing times all of us need to make more and more valuable contribution in the priority areas of our service for the society. Side by side, I feel, we should try to explore new vistas of service sectors as far as practicable.</p><p>In maintaining and developing international relation, activating highly reliable partnership with different sister clubs and in many service areas of noble human interest, our club has been sucessful to produce a host of landmarks. It is a matter of pride for us all. In these and other areas, I will be endevouring my level best to maintain and enhance the glorious strides of this club. No doubt, much has been done for the benefit of Nepalese people from the future vision program of the Rotary Foundation and it is heartening to mention-all of us, rotarians are moving ahead in genuine team spirit. Even so, we need to go on structuring the slots of time to make the range of our mission higher and wider.</p><p>Before giving a close to my words, I would also like to draw the attention of all, specifically the rotarians to keep in mind the superbly remarkable theme, \'LIGHT UP ROTARY\', forwarded by the President of the Rotary International for 2014/15, Mr, Gary C.K. Huang. Introducing the theme, \'LIGHT UP ROTARY\', respected Gary has given a new concept, \'Rotary Days\' which, I believe, will assist us more in our existing membership drive, strengthening our club\'s relationships with local institutions and community members as well as in improving the image of our club not only in local and national level but in international level as well.</p><p>Finally, I would like to invite all the fellow rotarians and people in general to come forward with open mind to share their noble ideas and spirit to widen the horizon of the service of this club for creating better situations of life in this part of the world and of course, keeping our minds and eyes open for the well being of the people in the international level too.</p><p>With bountiful thanks to you all,?</p>', 3, NULL, 'rtn-rama.jpg', 'District Governor', '2014 - 15', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:50:36', NULL),
	(164, 40, 'Club\'s Future Projects', NULL, 'clubs-future-projects', '<ul><li>1. Fishtail Fund (A long-term project running in 5th year)</li><li>2. Children of Annapurna Fund (A long-term project running in 3rd year)</li><li>3.Micro Loan Scheme(A long-term project initiated in2012)</li><li>4. Water Supply to Schools and Community at Majhthum, Bhadaure Tamagi, Kaski</li><li>5. Water Filtration project for schools, health institutes and community?</li></ul>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:02:43', NULL),
	(165, 40, 'Club\'s Long-term Projects', NULL, 'clubs-long-term-projects', '<p>Phasellus mattis tincidunt nibh.?<br></p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:03:39', NULL),
	(166, 40, 'Club\'s Past Projects', NULL, 'clubs-past-projects', '<ul><li>1. Water Supply project at Sahara Academy</li><li>2. Water Supply to Majhkot Village and Sivalaya Secondary School of Thumki</li><li>3. Water Supply to Dhruba Higher Secondary School, Tarkang, Kaski?</li></ul>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:04:45', NULL),
	(167, 41, 'MAJTHUM DRINKING WATER PROJECT HANDOVER', NULL, 'majthum-drinking-water-project-handover', '<p>Rotary Club of Pokhara Fishtail has decided to handover " Majthum Drinking Water Project" to the community on 6th December 2014.?<br></p>', 1, NULL, 'majthum1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:06:32', NULL),
	(168, 39, 'Wynne Castro', 'Non commodo sunt officia illo maiores cupidatat aut iure ut exercitationem ad et sit', 'wynne-castro', '<p>fdfdf</p>', 3, NULL, '', 'Sed qui doloremque molestiae ut incididunt repellendus Voluptate ab quo et et rerum magni cupidatat ', 'Rerum hic et ut eu eos eos', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:07:12', NULL),
	(169, 39, 'Hanna Tanner', 'Possimus enim id eu vel qui harum sit corporis lorem voluptas ullamco quasi atque quidem est reprehe', 'hanna-tanner', '<p>fdfdf</p>', 3, NULL, '', 'Incididunt ut et aut maxime pariatur Labore officiis modi est tempor fugit aliquip cupiditate et ass', 'In consequat Enim ipsam do adipisicing tempora', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:07:49', NULL),
	(170, 39, 'Rajah Jackson', 'Eos est laborum sunt aute magnam quos', 'rajah-jackson', '<p>fdsjfds</p>', 3, NULL, '', 'Laborum Nulla in inventore eum omnis enim esse eu corporis ut veniam error maxime qui unde temporibu', 'Voluptatum accusantium eos consectetur excepturi provident autem debitis explicabo Ut quas ab doloru', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:08:14', NULL),
	(171, 40, 'Giacomo Mcclain', NULL, 'giacomo-mcclain', '<p>fdfdsf</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:08:37', NULL),
	(172, 4, 'jk', NULL, 'jk', '<p>jlk</p>', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:38:47', NULL),
	(173, 40, 'Rhonda English', NULL, 'rhonda-english', '<p>kjlkj</p>', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:40:13', NULL),
	(174, 41, 'dfdlsj', NULL, 'dfdlsj', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:42:59', NULL),
	(175, 41, 'dell', NULL, 'dell', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:44:42', NULL),
	(176, 39, 'Drake Doylefdfd', NULL, 'drake-doylefdfd', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:45:19', NULL),
	(177, 4, 'Leo Hunt', NULL, 'leo-hunt', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:46:38', NULL),
	(178, 41, '12121', NULL, '12121', '<p>12121</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-28 19:42:49', NULL),
	(179, 41, 'isset', NULL, 'isset', '<p class="para rdfs-comment">Determine if a variable is set and is not <strong><code>NULL</code></strong>.</p>\r\n<p class="para">If a variable has been unset with <span class="function"><a class="function" href="http://php.net/manual/en/function.unset.php">unset()</a></span>, it will no longer be set. <span class="function"><strong>isset()</strong></span> will return <strong><code>FALSE</code></strong> if testing a variable that has been set to <strong><code>NULL</code></strong>. Also note that a null character (<em>"\\0"</em>) is not equivalent to the PHP <strong><code>NULL</code></strong> constant.</p>\r\n<p class="para">If multiple parameters are supplied then <span class="function"><strong>isset()</strong></span> will return <strong><code>TRUE</code></strong> only if all of the parameters are set. Evaluation goes from left to right and stops as soon as an unset variable is encountered.</p>', 1, NULL, '3-tips-ui-router-600x250.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-28 19:48:51', NULL),
	(180, 40, 'Brynn Spencer', NULL, 'brynn-spencer', '<p>df</p>', 3, NULL, NULL, 'Quam ipsa sint eius dolorem est', NULL, NULL, 'Quasi cum deserunt reprehenderit cum odio mollitia ut dolore illo sequi eu sit labore quia magni ani', NULL, 'Ea incidunt, ab aliquam eveniet, possimus, doloribus rerum explicabo. Consequatur.', 25, NULL, 'Commodi exercitation eveniet molestias quis id qui incidunt sunt id itaque id velit', 'Dolor lorem commodo blanditiis autem praesentium reprehenderit libero in aute', 'Aperiam et assumenda consectetur mollitia sint quod sed', NULL, NULL, '2015-08-28 20:03:42', NULL),
	(181, 41, 'n', NULL, 'n', '<p>c</p>', 3, NULL, NULL, 'it', NULL, NULL, 'vt', NULL, 'embed', 25, NULL, 'mk', 'md', 'mr', 'http://url', NULL, '2015-08-28 20:09:03', NULL),
	(182, 41, 'Nero Careye', NULL, 'nero-carey', '<p>cce</p>', 3, NULL, '232828-1406991727.jpg', 'iIllum consequat Dolores fuga Eligendi provident deserunt velit enim exercitation sunt suscipit vero', NULL, NULL, 'vFuga Voluptas incidunt itaque omnis rerum molestiae nobis velit', 'http://v', 'eNostrud voluptatem. Eaque vel delectus, cum nemo non tempore, consequat. Reprehenderit quo maiores ut assumenda accusantium.', 25, 25, 'kMaiores quo et repellendus Aut velit hic est amet a', 'dAut ab veniam necessitatibus voluptate', 'rLaborum placeat alias aut aut', 'http://uEsse et amet veniam minima non excepturi numquam ex non et ex sit qui irure delectus aut dig', NULL, '2015-08-28 20:09:45', '2015-08-28 17:55:57'),
	(183, 44, 'Kai Nolan', NULL, 'kai-nolan', '<p>test</p>', 1, NULL, NULL, 'Delectus sit reprehenderit cupidatat deserunt provident dolore dolorem dolore ex dolores doloribus n', NULL, NULL, 'Qui neque rerum aut autem est', 'http://Quae provident reprehenderit aut aut consequat Assumenda quia', 'Quam quia nobis dolores quia neque ea est, labore et suscipit quae laboriosam, tempora in reprehenderit itaque dolorem id.', 25, NULL, 'Et nulla obcaecati sed aperiam officia id tenetur velit et dolor totam exercitation nihil', 'Fugit anim voluptatem quam ducimus magnam duis dolore aliqua Do dolores Nam sit dolore delectus ad ut', 'Tempore ut dolores doloribus provident elit in aut enim pariatur Nihil ut non tenetur', 'http://Eum voluptatem sint doloremque ut ex ullamco est', NULL, '2015-08-29 16:02:45', NULL),
	(184, 41, 'Board of Directors 2014/15', NULL, 'board-of-directors-201415', '<table width="995">\r\n<tbody>\r\n<tr>\r\n<td><img src="../uploads/tinyMCE/main/member/Acr27052689167828-22888.jpg" alt="" width="262" height="327" /></td>\r\n<td><img  100px; margin-left: 100px;" src="../uploads/tinyMCE/main/member/Acr2705268916782819877.jpg" alt="" width="274" height="338" /></td>\r\n</tr>\r\n</tbody>\r\n</table>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-31 11:37:06', '2015-08-31 07:53:50'),
	(185, 41, 'test article', NULL, 'test-article', '<p><img src="../uploads/tinyMCE/main/Chrysanthemum.jpg" alt="" width="137" height="158" /></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-31 17:58:25', '2015-08-31 14:25:53'),
	(186, 38, 'Davis Jarvis', NULL, 'davis-jarvis', '<table  100px;" width="957">\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>2</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, NULL, NULL, 'Aut quam nesciunt in minima molestiae autem maiores', NULL, NULL, 'Et iste minim dolor voluptatem aut cum laborum Repellendus Sed illum architecto ullamco', 'http://Duis voluptates fuga Sed aute', 'Officia voluptatum doloremque assumenda illum, impedit, ratione eum dolore optio, et consequatur soluta cupidatat quia quia non excepturi itaque.', 25, 25, 'Voluptas est exercitationem ipsa occaecat voluptatum sint tempore ipsum eos ipsa non', 'Animi laboris nulla autem nisi quisquam laudantium officia distinctio Anim Nam sed quis exercitationem voluptatibus duis', 'Aute qui voluptas vel rerum dolore temporibus ullamco dolores magnam aut officiis iusto id consectet', 'http://Consectetur sunt accusantium sit blanditiis Nam similique consequatur ut officiis nobis commo', NULL, '2015-08-31 21:31:56', '2015-08-31 19:41:21'),
	(187, 39, 'Steven Levine', 'Unde nulla earum totam omnis ut saepe earum ratione quidem non', 'steven-levine', '<p>dfd</p>', 3, NULL, '', 'Eum ratione adipisci tempora quis voluptatem Corporis quia dolor quam mollit nisi cupidatat nostrum ', 'Provident blanditiis cillum non animi commodi quam neque laborum exercitation in hic cumque id conse', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-01 09:30:58', NULL),
	(188, 4, 'Andrew Hull', NULL, 'andrew-hull', '<p>dfdf</p>', 3, NULL, NULL, 'Eu voluptatem et vel omnis molestiae deleniti quam aute modi non in odio sed voluptas deserunt non', NULL, NULL, 'Nostrum voluptatem sit quo aut cumque cupiditate rerum quo ratione expedita consequatur excepturi om', 'http://Recusandae Rerum natus ad assumenda ut irure molestiae ut omnis mollitia sunt ipsam atque consequuntur magni tenetur deserunt reprehenderit velit', 'Distinctio. Esse, eiusmod omnis consectetur, dolorem aliquid debitis proident, est, possimus, Nam maiores ipsum enim quia et.', 25, NULL, 'Vero enim omnis dolores pariatur Quis porro iure laborum Ad magnam', 'Obcaecati molestiae porro et ipsam dignissimos expedita id doloribus qui soluta ea fuga Totam minima impedit', 'Voluptatibus amet cumque voluptatem Velit commodo saepe molestiae Nam molestiae dolores do nesciunt ', 'http://Voluptas veniam mollit suscipit fugiat voluptas voluptate aut nihil voluptatem officia sint n', NULL, '2015-09-01 09:31:33', NULL);
/*!40000 ALTER TABLE `tbl_articles` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_campaign
CREATE TABLE IF NOT EXISTS `tbl_campaign` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `fund_category_id` int(22) DEFAULT NULL,
  `campaign_title` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `target_amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(25000) NOT NULL,
  `video` varchar(111) NOT NULL,
  `document` varchar(111) DEFAULT NULL,
  `fb_link` varchar(111) NOT NULL,
  `twitter_link` varchar(111) NOT NULL,
  `enable_comment` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `starting_at` date NOT NULL,
  `ending_at` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url_link` varchar(50) DEFAULT NULL,
  `success_story` varchar(20000) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `fund_category_id` (`fund_category_id`),
  CONSTRAINT `FK_tbl_campaign_tbl_users` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_campaign: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_campaign` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_categories
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(25) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `image_title` varchar(50) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `published` tinyint(3) unsigned DEFAULT NULL,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`name`),
  KEY `tbl_categories_ibfk_1` (`parent_id`),
  KEY `tbl_categories_ibfk_2` (`author`),
  KEY `tbl_categories_ibfk_3` (`modified_by`),
  CONSTRAINT `FK_tbl_categories_tbl_categories` FOREIGN KEY (`parent_id`) REFERENCES `tbl_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_categories_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_categories_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_categories: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_categories` DISABLE KEYS */;
INSERT INTO `tbl_categories` (`id`, `parent_id`, `name`, `slug`, `content`, `image`, `image_title`, `url`, `order`, `published`, `author`, `modified_by`, `created_at`, `updated_at`, `status`) VALUES
	(4, NULL, 'uncategoried', 'uncategoried', 'uncategoried', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(38, NULL, 'Help', 'help', 'help', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 2),
	(39, NULL, 'messages', 'messages', 'help', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(40, NULL, 'projects', 'projects', 'projects', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(41, NULL, 'news', 'news', 'news', '179.jpg', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(42, NULL, 'Aubrey Kline', 'aubrey-kline', '<p>e</p>', '235397-1408974243.gif', 'Possimus voluptate est odio sed veniam esse vitae ', 'Occaecat corrupti enim qui minim delenitie', NULL, NULL, 25, NULL, '2015-08-29 13:37:41', NULL, 2),
	(43, NULL, 'Robert Cranen', 'robert-cranen', '<p>cdfdf</p>', 'angular-forms-300x125.jpg', 'itAnimi voluptatem Veniam explicabo Inventore et q', 'uCulpa ea asperiores dolore autem quod aliquam non harum culpa eum voluptate', NULL, NULL, 25, NULL, '2015-08-29 13:55:06', NULL, 2),
	(44, NULL, 'Renee Howe', 'renee-howe', '<p>e</p>', '', 'Odit fuga Et quia minima rerum', 'Consequatur Accusamus sunt corporis et ducimus nulla deserunt proident sed', NULL, NULL, 25, NULL, '2015-08-29 16:01:56', NULL, 2),
	(45, NULL, 'slider', 'slider', '', '', '', '', NULL, NULL, 25, NULL, '2015-09-01 19:49:52', NULL, 1);
/*!40000 ALTER TABLE `tbl_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donar
CREATE TABLE IF NOT EXISTS `tbl_donar` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donar: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donar` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donation
CREATE TABLE IF NOT EXISTS `tbl_donation` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `donar_id` int(22) NOT NULL,
  `campaign_id` int(22) NOT NULL,
  `amount` int(22) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donar_id` (`donar_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donation: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donation` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_fund_categories
CREATE TABLE IF NOT EXISTS `tbl_fund_categories` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `glyphicon` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_fund_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_fund_categories` DISABLE KEYS */;
INSERT INTO `tbl_fund_categories` (`id`, `name`, `slug`, `description`, `image`, `glyphicon`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'fund_category', 'fund_category', 'Provident, nesciunt, a cupiditate praesentium ut aut maxime reprehenderit, quia culpa quis repudiandae eiusmod quia pariatur? Nisi.', '', 'fa fa-heart', 1, '2015-06-27 18:06:03', '2015-07-01 04:57:41'),
	(2, 'Jaime Gibson', 'jaime-gibson', 'Sint nisi minima sit elit, sed tenetur irure duis aliquid velit quis saepe aliquam et enim aut nesciunt, deleniti.', '', 'Voluptas eius commodi facere dolor', 2, '2015-07-15 19:26:57', NULL);
/*!40000 ALTER TABLE `tbl_fund_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_groups
CREATE TABLE IF NOT EXISTS `tbl_groups` (
  `id` int(22) unsigned NOT NULL AUTO_INCREMENT,
  `parent_group_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_groups_ibfk_1` (`parent_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_groups: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_groups` DISABLE KEYS */;
INSERT INTO `tbl_groups` (`id`, `parent_group_id`, `name`, `slug`, `desc`, `status`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Superadmin', 'superadmin', 'superadmin desc', 1, '2015-01-28 00:36:41', NULL),
	(2, 1, 'Admin', 'admin', '', 1, '2015-01-28 00:37:01', NULL),
	(3, NULL, 'Donee', 'donee', 'Quibusdam at sint voluptas eum debitis accusamus voluptatem voluptatem, non eu et in et illum, ab et sed ut.', 1, '2015-03-28 13:22:30', NULL),
	(4, NULL, 'Facebook User', 'facebook_user', 'd\r\n', 1, '2015-05-19 17:00:54', NULL);
/*!40000 ALTER TABLE `tbl_groups` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_group_permissions
CREATE TABLE IF NOT EXISTS `tbl_group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `permission_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_group_permissions_ibfk_1` (`group_id`),
  KEY `tbl_group_permissions_ibfk_2` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3780 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_group_permissions: ~79 rows (approximately)
/*!40000 ALTER TABLE `tbl_group_permissions` DISABLE KEYS */;
INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(1749, 8, 80, '2015-06-08 15:07:15', NULL),
	(1750, 8, 82, '2015-06-08 15:07:15', NULL),
	(1751, 8, 83, '2015-06-08 15:07:15', NULL),
	(1752, 8, 84, '2015-06-08 15:07:15', NULL),
	(1753, 8, 85, '2015-06-08 15:07:15', NULL),
	(1754, 8, 86, '2015-06-08 15:07:15', NULL),
	(3665, 2, 5, '2015-06-27 21:42:35', NULL),
	(3666, 2, 6, '2015-06-27 21:42:35', NULL),
	(3667, 2, 7, '2015-06-27 21:42:35', NULL),
	(3668, 2, 96, '2015-06-27 21:42:35', NULL),
	(3669, 2, 97, '2015-06-27 21:42:35', NULL),
	(3670, 2, 98, '2015-06-27 21:42:35', NULL),
	(3671, 2, 99, '2015-06-27 21:42:35', NULL),
	(3672, 2, 55, '2015-06-27 21:42:35', NULL),
	(3673, 2, 61, '2015-06-27 21:42:35', NULL),
	(3674, 2, 64, '2015-06-27 21:42:35', NULL),
	(3675, 2, 65, '2015-06-27 21:42:35', NULL),
	(3676, 2, 66, '2015-06-27 21:42:36', NULL),
	(3677, 2, 67, '2015-06-27 21:42:36', NULL),
	(3678, 2, 68, '2015-06-27 21:42:36', NULL),
	(3679, 2, 70, '2015-06-27 21:42:36', NULL),
	(3680, 2, 72, '2015-06-27 21:42:36', NULL),
	(3681, 2, 73, '2015-06-27 21:42:36', NULL),
	(3682, 2, 74, '2015-06-27 21:42:36', NULL),
	(3683, 2, 76, '2015-06-27 21:42:36', NULL),
	(3684, 2, 77, '2015-06-27 21:42:36', NULL),
	(3685, 2, 78, '2015-06-27 21:42:36', NULL),
	(3686, 2, 79, '2015-06-27 21:42:36', NULL),
	(3687, 2, 80, '2015-06-27 21:42:36', NULL),
	(3688, 2, 82, '2015-06-27 21:42:36', NULL),
	(3689, 2, 83, '2015-06-27 21:42:36', NULL),
	(3690, 2, 84, '2015-06-27 21:42:36', NULL),
	(3691, 2, 85, '2015-06-27 21:42:36', NULL),
	(3692, 2, 86, '2015-06-27 21:42:36', NULL),
	(3693, 2, 100, '2015-06-27 21:42:36', NULL),
	(3694, 2, 91, '2015-06-27 21:42:36', NULL),
	(3737, 1, 5, '2015-07-31 22:40:16', NULL),
	(3738, 1, 6, '2015-07-31 22:40:16', NULL),
	(3739, 1, 7, '2015-07-31 22:40:16', NULL),
	(3740, 1, 96, '2015-07-31 22:40:16', NULL),
	(3741, 1, 97, '2015-07-31 22:40:16', NULL),
	(3742, 1, 98, '2015-07-31 22:40:16', NULL),
	(3743, 1, 99, '2015-07-31 22:40:16', NULL),
	(3744, 1, 8, '2015-07-31 22:40:17', NULL),
	(3745, 1, 9, '2015-07-31 22:40:17', NULL),
	(3746, 1, 10, '2015-07-31 22:40:17', NULL),
	(3747, 1, 17, '2015-07-31 22:40:17', NULL),
	(3748, 1, 15, '2015-07-31 22:40:17', NULL),
	(3749, 1, 16, '2015-07-31 22:40:17', NULL),
	(3750, 1, 87, '2015-07-31 22:40:17', NULL),
	(3751, 1, 88, '2015-07-31 22:40:17', NULL),
	(3752, 1, 89, '2015-07-31 22:40:17', NULL),
	(3753, 1, 90, '2015-07-31 22:40:17', NULL),
	(3754, 1, 92, '2015-07-31 22:40:17', NULL),
	(3755, 1, 93, '2015-07-31 22:40:17', NULL),
	(3756, 1, 55, '2015-07-31 22:40:17', NULL),
	(3757, 1, 61, '2015-07-31 22:40:17', NULL),
	(3758, 1, 64, '2015-07-31 22:40:17', NULL),
	(3759, 1, 65, '2015-07-31 22:40:17', NULL),
	(3760, 1, 66, '2015-07-31 22:40:17', NULL),
	(3761, 1, 67, '2015-07-31 22:40:17', NULL),
	(3762, 1, 68, '2015-07-31 22:40:17', NULL),
	(3763, 1, 70, '2015-07-31 22:40:17', NULL),
	(3764, 1, 72, '2015-07-31 22:40:17', NULL),
	(3765, 1, 73, '2015-07-31 22:40:18', NULL),
	(3766, 1, 74, '2015-07-31 22:40:18', NULL),
	(3767, 1, 76, '2015-07-31 22:40:18', NULL),
	(3768, 1, 77, '2015-07-31 22:40:18', NULL),
	(3769, 1, 78, '2015-07-31 22:40:18', NULL),
	(3770, 1, 79, '2015-07-31 22:40:18', NULL),
	(3771, 1, 101, '2015-07-31 22:40:18', NULL),
	(3772, 1, 80, '2015-07-31 22:40:18', NULL),
	(3773, 1, 82, '2015-07-31 22:40:18', NULL),
	(3774, 1, 83, '2015-07-31 22:40:18', NULL),
	(3775, 1, 84, '2015-07-31 22:40:18', NULL),
	(3776, 1, 85, '2015-07-31 22:40:18', NULL),
	(3777, 1, 86, '2015-07-31 22:40:18', NULL),
	(3778, 1, 100, '2015-07-31 22:40:18', NULL),
	(3779, 1, 91, '2015-07-31 22:40:18', NULL);
/*!40000 ALTER TABLE `tbl_group_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_logs
CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `log_type` varchar(111) NOT NULL,
  `log_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_logs` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_menus
CREATE TABLE IF NOT EXISTS `tbl_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `page_type_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `article_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) DEFAULT '0',
  `order` int(10) unsigned DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `sidebar` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_menus_ibfk_1` (`parent_id`),
  KEY `tbl_menus_ibfk_2` (`author`),
  KEY `tbl_menus_ibfk_3` (`modified_by`),
  KEY `tbl_menus_ibfk_4` (`page_type_id`),
  KEY `tbl_menus_ibfk_5` (`category_id`),
  KEY `FK_tbl_menus_tbl_articles` (`article_id`),
  CONSTRAINT `FK_tbl_menus_tbl_articles` FOREIGN KEY (`article_id`) REFERENCES `tbl_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_menus` FOREIGN KEY (`parent_id`) REFERENCES `tbl_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_page_types` FOREIGN KEY (`page_type_id`) REFERENCES `tbl_page_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_menus: ~33 rows (approximately)
/*!40000 ALTER TABLE `tbl_menus` DISABLE KEYS */;
INSERT INTO `tbl_menus` (`id`, `parent_id`, `page_type_id`, `category_id`, `article_id`, `name`, `level`, `order`, `slug`, `desc`, `status`, `author`, `modified_by`, `created_at`, `updated_at`, `sidebar`) VALUES
	(1, NULL, 1, NULL, 117, 'Home', 0, 1, 'home', 'home desc', 2, 25, NULL, '2015-03-02 02:24:54', NULL, 'N'),
	(10, 15, 2, NULL, NULL, 'page dfd', 0, 9, 'page-1', '', 4, 25, NULL, '2015-03-03 02:36:03', NULL, NULL),
	(11, 10, 2, 4, NULL, 'page one', 1, 10, 'page-11', '', 4, 25, NULL, '2015-03-03 02:36:24', NULL, NULL),
	(13, 10, 2, 4, NULL, 'page one two', 1, 13, 'page-12', '', 2, 25, NULL, '2015-03-03 02:36:54', NULL, NULL),
	(14, 11, 2, NULL, NULL, 'page 1.1.1', 2, 11, 'page-111', '', 2, 25, NULL, '2015-03-03 02:37:16', NULL, NULL),
	(15, 39, 2, 4, NULL, 'Long-term Project', 1, 11, 'page-121', '', 2, 25, NULL, '2015-03-03 02:37:38', NULL, 'N'),
	(16, 13, 2, NULL, NULL, 'page 1.2.2', 2, 15, 'page-122', '', 2, 25, NULL, '2015-03-03 02:38:04', NULL, NULL),
	(17, NULL, 1, NULL, NULL, 'n', 0, 1, 'n', 'D', 4, 25, NULL, '2015-03-14 00:54:48', NULL, 'Y'),
	(18, NULL, 9, NULL, NULL, 'about us', 0, 2, 'about-us', '', 2, 25, NULL, '2015-03-16 21:32:55', NULL, 'Y'),
	(19, NULL, 2, NULL, NULL, 'Club Members', 0, 7, 'courses', '', 2, 25, NULL, '2015-03-16 21:33:11', NULL, 'N'),
	(25, NULL, 1, NULL, NULL, 'Bulletins', 0, 15, 'admission', '', 2, 25, NULL, '2015-03-16 21:37:14', NULL, 'N'),
	(26, 18, 1, 39, NULL, 'Mission & Objectives', 1, 4, 'services', '', 2, 25, NULL, '2015-03-16 21:37:27', NULL, 'Y'),
	(27, NULL, 4, 41, NULL, 'News', 0, 14, 'news-events', '', 2, 25, NULL, '2015-03-16 21:37:41', NULL, 'N'),
	(28, 14, 1, NULL, NULL, 'page 1.1.1.1', 3, 12, 'page-1111', '', 2, 25, NULL, '2015-03-16 22:01:34', NULL, NULL),
	(29, 19, 2, NULL, NULL, 'Board of Directors', 1, 8, 'diploma-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:19', NULL, 'N'),
	(31, 19, 2, 40, NULL, 'Active Members', 1, 9, 'advanced-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:51', NULL, 'N'),
	(32, 19, 1, 43, NULL, 'Courses 3', 1, 16, 'courses-3', '', 1, 25, NULL, '2015-03-20 00:18:53', NULL, NULL),
	(33, NULL, 3, 40, NULL, 'Gallery', 0, 13, 'gallery', '', 2, 25, NULL, '2015-03-20 00:45:09', NULL, NULL),
	(35, 39, 1, NULL, NULL, 'Past Projects', 1, 12, 'downloads', '', 2, 25, NULL, '2015-03-20 00:48:57', NULL, 'N'),
	(36, NULL, 6, NULL, 9, 'Contact', 0, 16, 'contact', '', 2, 25, NULL, '2015-03-20 00:58:08', NULL, NULL),
	(37, 18, 1, NULL, NULL, 'Club Profile', 1, 5, 'new-menu', 'Voluptatum tempore, pariatur? Illum, nihil numquam soluta similique rerum aut aut dignissimos enim at aliquam et consectetur sed iste harum.', 2, 25, NULL, '2015-03-28 13:10:50', NULL, 'N'),
	(38, NULL, 2, 39, NULL, 'Club\'s Wings', 0, 6, 'chip-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:06', NULL, 'N'),
	(39, NULL, 1, 39, NULL, 'Service Projects', 0, 10, 'cretification-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:20', NULL, 'N'),
	(40, 40, 7, 41, 182, 'nabout chals', 0, 15, 'about-chals', 'm', 2, 25, NULL, '2015-05-24 20:31:10', NULL, NULL),
	(41, 46, 7, 40, 166, 'Laura Carson', 1, 18, 'laura-carson', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', 2, 25, NULL, '2015-08-30 23:05:53', NULL, NULL),
	(42, NULL, 7, 40, 166, 'Laura Carsone', 0, 25, 'laura-carsone', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', NULL, 25, NULL, '2015-08-30 23:16:25', NULL, NULL),
	(43, 18, 6, 38, 64, 'Diana Brock', 1, 25, 'diana-brock', 'Et itaque saepe enim fugiat, repudiandae eum laboris sit sint, quod odit atque sint.', NULL, 25, NULL, '2015-08-30 23:19:56', NULL, NULL),
	(44, NULL, 3, 38, 64, 'Emma Acevedo', 2, 18, 'emma-acevedo', 'Soluta odit qui porro dolorem aut duis consequatur? Accusamus minim aut ex fugit, et tempore, aut voluptate aute eligendi voluptas.', 4, 25, NULL, '2015-08-30 23:20:26', NULL, NULL),
	(45, 46, 4, 41, 166, 'Iliana Roberts', 1, 19, 'iliana-roberts', 'Sit molestias maxime culpa, quo veritatis numquam et reiciendis voluptatem irure.', 2, 25, NULL, '2015-08-30 23:21:00', NULL, NULL),
	(46, NULL, 2, 4, NULL, 'new pages', 0, 17, 'new-pages', '', 3, 25, NULL, '2015-08-30 23:51:47', NULL, 'N'),
	(47, 18, 1, 41, 51, 'introduction', 1, 3, 'colton-harmon', 'Ut fugiat, dolor eligendi iste ullamco voluptas beatae laboris pariatur? Repellendus. Non consequatur quos doloribus.', 2, 25, NULL, '2015-08-31 01:02:37', NULL, 'N'),
	(48, 46, 7, 39, 164, 'Zephr Simpson', 1, 20, 'zephr-simpson', 'Consequatur a voluptas ea doloribus voluptas soluta consequatur, inventore veniam, sit, qui nulla blanditiis voluptate sapiente magni.', 2, 25, NULL, '2015-08-31 01:03:10', NULL, 'N'),
	(49, 46, 2, 39, 161, 'Ingrid Mann', 1, 21, 'ingrid-mann', 'Iste perspiciatis, perferendis officia sint, sit, consequatur? Exercitation dolorum adipisicing dolore et exercitationem esse, nobis aliquam consequatur facilis.', 2, 25, NULL, '2015-08-31 01:09:15', NULL, 'N');
/*!40000 ALTER TABLE `tbl_menus` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_page_types
CREATE TABLE IF NOT EXISTS `tbl_page_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_page_types: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_page_types` DISABLE KEYS */;
INSERT INTO `tbl_page_types` (`id`, `name`, `desc`) VALUES
	(1, 'article', NULL),
	(2, 'category', NULL),
	(3, 'gallery', NULL),
	(4, 'news', NULL),
	(5, 'picture', NULL),
	(6, 'contact', NULL),
	(7, 'timeline', NULL),
	(8, '404', NULL),
	(9, 'blank', NULL);
/*!40000 ALTER TABLE `tbl_page_types` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_permissions
CREATE TABLE IF NOT EXISTS `tbl_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_permission_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_parent_permission_id` (`parent_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_permissions: ~62 rows (approximately)
/*!40000 ALTER TABLE `tbl_permissions` DISABLE KEYS */;
INSERT INTO `tbl_permissions` (`id`, `parent_permission_id`, `name`, `slug`, `desc`, `created_at`) VALUES
	(5, NULL, 'Administrator User', 'administrator-user', '', '2015-01-29 00:56:32'),
	(6, 5, 'add user', 'add-user', '', '2015-01-29 00:58:23'),
	(7, 5, 'edit user', 'edit-user', '', '2015-01-29 00:58:38'),
	(8, NULL, 'Administrator  Permission', 'administrator-permission', '', '2015-01-29 00:59:51'),
	(9, 8, 'add Permission', 'add-permission', '', '2015-01-29 01:00:02'),
	(10, 8, 'edit Permission', 'edit-permission', '', '2015-01-29 01:00:11'),
	(15, NULL, 'Administrator Group', 'administrator-group', '', '2015-02-01 15:40:08'),
	(16, 15, 'Add Group', 'add-group', '', '2015-02-01 15:45:09'),
	(17, 8, 'delete Permission', 'delete-permission', '', '2015-02-20 22:21:45'),
	(31, 30, 'list category', 'list-category', '', '2015-02-23 01:40:17'),
	(32, 30, 'add category', 'add-category', '', '2015-02-23 01:43:00'),
	(33, 30, 'edit category', 'edit-category', '', '2015-02-23 01:43:17'),
	(34, 30, 'delete category', 'delete-category', '', '2015-02-23 01:43:27'),
	(35, 30, 'activate category', 'activate-category', '', '2015-02-23 01:43:35'),
	(36, 30, 'block category', 'block-category', '', '2015-02-23 01:43:43'),
	(38, 37, 'list menu', 'list-menu', '', '2015-02-23 01:40:17'),
	(39, 37, 'add menu', 'add-menu', '', '2015-02-23 01:43:00'),
	(40, 37, 'edit menu', 'edit-menu', '', '2015-02-23 01:43:17'),
	(41, 37, 'delete menu', 'delete-menu', '', '2015-02-23 01:43:27'),
	(42, 37, 'activate menu', 'activate-menu', '', '2015-02-23 01:43:35'),
	(43, 37, 'block menu', 'block-menu', '', '2015-02-23 01:43:43'),
	(48, 47, 'add article', 'add-article', '', '2015-02-23 01:43:00'),
	(49, 47, 'edit article', 'edit-article', '', '2015-02-23 01:43:17'),
	(50, 47, 'delete article', 'delete-article', '', '2015-02-23 01:43:27'),
	(51, 47, 'activate article', 'activate-article', '', '2015-02-23 01:43:35'),
	(52, 47, 'block article', 'block-article', '', '2015-02-23 01:43:43'),
	(53, 47, 'list article', 'list-article', '', '2015-02-23 01:40:17'),
	(54, 47, 'view article', 'view-article', '', '2015-02-23 01:40:17'),
	(55, NULL, 'administrator setting', 'administrator-setting', '', '2015-01-29 00:56:32'),
	(61, NULL, 'administrator fund category', 'administrator-fund-category', '', '2015-05-24 21:40:30'),
	(64, 61, 'add-fund-category', 'add-fund-category', '', '2015-05-24 21:40:30'),
	(65, 61, 'edit-fund-category', 'edit-fund-category', '', '2015-05-24 21:40:30'),
	(66, 61, 'delete-fund-category', 'delete-fund-category', '', '2015-05-24 21:40:30'),
	(67, 61, 'activate-fund-category', 'activate-fund-category', '', '2015-05-24 21:40:30'),
	(68, 61, 'block-fund-category', 'block-fund-category', '', '2015-05-24 21:40:30'),
	(70, 61, 'list-fund-category', 'list-fund-category', '', '2015-05-24 21:40:30'),
	(72, NULL, 'administrator-campaign', 'administrator-campaign', '', '2015-05-24 21:40:30'),
	(73, 72, 'add-campaign', 'add-campaign', '', '2015-05-24 21:40:30'),
	(74, 72, 'edit-campaign', 'edit-campaign', '', '2015-05-24 21:40:30'),
	(76, 72, 'delete-campaign', 'delete-campaign', '', '2015-05-24 21:40:30'),
	(77, 72, 'activate-campaign', 'activate-campaign', '', '2015-05-24 21:40:30'),
	(78, 72, 'block-campaign', 'block-campaign', '', '2015-05-24 21:40:30'),
	(79, 72, 'list-campaign', 'list-campaign', '', '2015-05-24 21:40:30'),
	(80, NULL, 'administrator-donee', 'administrator-donee', '', '2015-06-07 21:43:27'),
	(82, 80, 'list-donee', 'list-donee', '', '2015-06-07 21:44:53'),
	(83, 80, 'add-donee', 'add-donee', '', '2015-06-07 21:45:20'),
	(84, 80, 'activate-donee', 'activate-donee', '', '2015-06-07 21:45:43'),
	(85, 80, 'block-donee', 'block-donee', '', '2015-06-07 21:45:56'),
	(86, 80, 'delete-donee', 'delete-donee', '', '2015-06-07 21:46:07'),
	(87, 15, 'edit group', 'edit-group', '', '2015-06-08 08:51:37'),
	(88, 15, 'activate-group', 'activate-group', '', '2015-06-08 14:52:22'),
	(89, 15, 'block-group', 'block-group', '', '2015-06-08 14:52:37'),
	(90, 15, 'delete-group', 'delete-group', '', '2015-06-08 14:52:54'),
	(91, NULL, 'administrator-donation', 'administrator-donation', '', '2015-06-07 21:43:27'),
	(92, 15, 'update-permission-group', 'update-permission-group', '', '2015-06-19 09:44:57'),
	(93, 15, 'List Group', 'list-group', '', '2015-06-19 09:46:11'),
	(96, 5, 'activate user', 'activate-user', '', '2015-01-29 00:58:38'),
	(97, 5, 'block user', 'block-user', '', '2015-01-29 00:58:38'),
	(98, 5, 'delete user', 'delete-user', '', '2015-01-29 00:58:38'),
	(99, 5, 'list user', 'list-user', '', '2015-01-29 00:58:38'),
	(100, 80, 'edit-donee', 'edit-donee', '', '2015-06-20 18:40:28'),
	(101, 72, 'success-campaign', 'success-campaign', 'success-campaign desc', '2015-07-31 22:38:31');
/*!40000 ALTER TABLE `tbl_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_settings
CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `value` longtext NOT NULL,
  `autoload` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_settings: ~39 rows (approximately)
/*!40000 ALTER TABLE `tbl_settings` DISABLE KEYS */;
INSERT INTO `tbl_settings` (`id`, `name`, `slug`, `value`, `autoload`) VALUES
	(1, 'email', 'email', 'info@ourlibraray.com', 1),
	(2, 'cell', 'cell', '9806677215', 0),
	(3, 'address', 'address', 'Melbourne , Austrailia', 0),
	(5, 'facebook_link', 'facebook_link', 'https://www.facebook.com/ourlibrary.com', 0),
	(6, 'twitter_link', 'twitter_link', 'https://www.twitter.com/ourlibrary.com', 0),
	(7, 'google_plus_link', 'google_plus_link', 'https://www.googleplus.com/ourlibrary.com', 0),
	(8, 'Site Name', 'site_name', 'Rotary Club Of Pokhara Fishtail', 0),
	(9, 'Site Url', 'site_url', 'Rotary Club Of Pokhara Fishtail', 0),
	(12, 'club_name', 'club_name', 'Rotary Club Of Pokhara Fishtail', 1),
	(13, 'club_number', 'club_number', 'Club No. 63638', 1),
	(15, 'slogan_head', 'slogan_head', 'Service above Self', 1),
	(16, 'slogan_body', 'slogan_body', 'ROTARY INTERNATIONAL', 1),
	(17, 'slogan_dist', 'slogan_dist', 'Dist. 32921', 1),
	(18, 'other_site_1_name', 'other_site_1_name', 'Fishtail Fund', 1),
	(19, 'other_site_1_url', 'other_site_1_url', 'http://www.fishtailfund.com/', 1),
	(20, 'other_site_2_name', 'other_site_2_name', 'Annapurna Fund', 1),
	(21, 'other_site_2_url', 'other_site_2_url', 'http://www.fishtailfund.np/e', 1),
	(22, 'slider_show_total', 'slider_show_total', '5', 1),
	(23, 'sidebar1_content', 'sidebar1_content', 'Info\r\n[We meet on every Friday]\r\nat Atithi Resort & Spa\r\nLakeside, Pokhara, Nepal.\r\nFeb-Oct. (6:00pm to 7:00pm) \r\nNov-Jan. (5:30pm to 6:30pm).', 1),
	(24, 'sidebar2_content', 'sidebar2_content', 'NOTICE\r\n[We meet on every Friday]\r\nat Atithi Resort & Spa\r\nLakeside, Pokhara, Nepal.\r\nFeb-Oct. (6:00pm to 7:00pm) \r\nNov-Jan. (5:30pm to 6:30pm).', 1),
	(25, 'footer_message', 'footer_message', ' 2013-2015 | Rotary Club of Fishtail Pokhara. All Rights Reserved. ', 1),
	(26, 'slider_category_id', 'slider_category_id', '45', 1),
	(27, 'telephone_1', 'telephone_1', '+977 98560-21476', 1),
	(28, 'telephone_2', 'telephone_2', 't2', 1),
	(29, 'mobile_1', 'mobile_1', 'm1', 1),
	(30, 'mobile_2', 'mobile_2', 'm2', 1),
	(31, 'email_1', 'email_1', 'info@rotaryfishtail.org', 1),
	(32, 'email_2', 'email_2', 'e2', 1),
	(33, 'location', 'location', 'Rotary Club of Pokhara Fishtail, Atithi Resort & Spa, Lakeside, Pokhara, Nepal', 1),
	(34, 'contact', 'contact', 'Submit', 1),
	(41, 'header_logo', 'header_logo', '3-tips-ui-router-600x250.jpg', 1),
	(42, 'sidebar1_picture', 'sidebar1_picture', '246be9cf59f9eb8dbfae22c7a38f917d.jpg', 1),
	(43, 'sidebar2_picture', 'sidebar2_picture', '232828-1406991727.jpg', 1),
	(44, 'no_of_message_on_home_page', 'no_of_message_on_home_page', '3', 1),
	(46, 'no_of_projects_on_home_page', 'no_of_projects_on_home_page', '2', 1),
	(49, 'message_category_id', 'message_category_id', '39', 1),
	(61, 'project_category_id', 'project_category_id', '40', 1),
	(62, 'no_of_news_on_home_page', 'no_of_news_on_home_page', '31', 1),
	(67, 'news_category_id', 'news_category_id', '41', 1),
	(68, 'welcome_message', 'welcome_message', 'TO ROTARY DIST-3292 OFFICIAL WEBSITE', 1),
	(69, 'introduction', 'introduction', 'Rotary International is a voluntary organization of business and professional leaders which provide humanitarian servicesone to help to build goodwill and peace in the world. There are more than 1.2 million (12 Lakhs) members 34000 Rotary clubs in over 200 countries and geographical areas. [Rotary\'s top priority is the global graphical eradication of polio.] Founded in Chicago in 1905, The Rotary foundation has awarded more than U.S. $ 2.1 billion in grants, which are administered at the local level by Rotary Clubs.', 1),
	(70, 'introduction_article_id', 'introduction_article_id', '53', 1);
/*!40000 ALTER TABLE `tbl_settings` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `facebook_id` bigint(20) DEFAULT NULL,
  `username` varchar(111) NOT NULL,
  `group_id` int(22) unsigned NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `pass` varchar(111) NOT NULL,
  `address_unit` varchar(111) NOT NULL,
  `address_street` varchar(111) NOT NULL,
  `address_suburb` varchar(111) NOT NULL,
  `address_state_id` int(11) NOT NULL,
  `contact_emails` varchar(111) NOT NULL,
  `status` int(11) NOT NULL,
  `verification_code` varchar(111) NOT NULL,
  `last_login_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_id` (`facebook_id`,`username`),
  KEY `FK_tbl_users_tbl_groups` (`group_id`),
  CONSTRAINT `FK_tbl_users_tbl_groups` FOREIGN KEY (`group_id`) REFERENCES `tbl_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`id`, `facebook_id`, `username`, `group_id`, `first_name`, `last_name`, `email`, `pass`, `address_unit`, `address_street`, `address_suburb`, `address_state_id`, `contact_emails`, `status`, `verification_code`, `last_login_date`, `created_at`, `updated_at`) VALUES
	(25, NULL, 'superadmin', 1, 'ram', 'bahadur', 'raxizel@gmai.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pokhara', 'kajipokhri', 'suburb', 0, '', 1, '', '2015-05-09', '2015-05-09 00:00:00', '2015-05-09 00:00:00'),
	(181, NULL, 'admin', 1, 'ramesh', 'bahadur', 'raxizel@gmai.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pokhara', 'kajipokhri', 'suburb', 0, '', 1, '', '2015-05-09', '2015-05-09 00:00:00', '2015-05-09 00:00:00');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
