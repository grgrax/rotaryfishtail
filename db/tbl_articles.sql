-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2015 at 09:01 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_rotaryfishtail_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_articles`
--

CREATE TABLE IF NOT EXISTS `tbl_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `order` tinyint(3) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `image_title_2` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `video_title` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `embed_code` varchar(10000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `meta_key` varchar(100) DEFAULT NULL,
  `meta_desc` varchar(10000) DEFAULT NULL,
  `meta_robots` varchar(100) DEFAULT NULL,
  `url1` varchar(100) DEFAULT NULL,
  `url2` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_articles_ibfk_1` (`category_id`),
  KEY `tbl_articles_ibfk_3` (`author`),
  KEY `tbl_articles_ibfk_4` (`modified_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `tbl_articles`
--

INSERT INTO `tbl_articles` (`id`, `category_id`, `order`, `name`, `status`, `title`, `slug`, `content`, `image`, `image_title`, `image_title_2`, `video`, `video_title`, `video_url`, `embed_code`, `created_at`, `author`, `modified_by`, `meta_key`, `meta_desc`, `meta_robots`, `url1`, `url2`, `updated_at`) VALUES
(51, 4, NULL, 'how it works', 1, NULL, 'how-it-works', '<ol><li><b><h2>step 1</h2></b><p>ok man</p></li><li><b><h2>step 2</h2></b><p>yes man</p></li><li><b><h2>step 2</h2></b><p>be ready</p></li></ol>', '524440_4362996394967_145472913_n.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 08:36:41', 25, 25, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(52, 38, NULL, 'How can i create Campaign here', 1, NULL, 'how-can-i-create-campaign-here', '<p>Just sign up and your campaign is ready</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 08:37:28', 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:03:15'),
(53, 4, NULL, 'about us', 1, NULL, 'about-us', '<p>Ourlibrary.com.au is fundraising website</p><p>This is dummy text for our library website. This is dummy text for our library website. This is dummy text for our library website.This is dummy text for our library website.This is dummy text for our library website. This is dummy text for our library website. This is dummy text for our library website.This is dummy text for our library website.This is dummy text for our library website.<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 08:37:43', 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 13:32:46'),
(54, 4, NULL, 'useful links', 1, NULL, 'useful-links', '<ul><li><a target="_blank" rel="nofollow" href="http://www.google.com">List items 1</a></li><li>List items 2</li><li>List items 3</li><li>list items 4</li></ul><br><br>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 08:38:12', 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 17:59:03'),
(57, 38, NULL, 'What is Ourlibrary.com.au', 1, NULL, 'what-is-ourlibrarycomau', 'Ourlibrary.com.au is fundraising website&nbsp;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 08:37:28', 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:03:12'),
(60, 38, NULL, 'How i will receive raised donation once campaign get completed?', 1, NULL, 'how-i-will-receive-raised-donation-once-campaign-get-completed', '<p>You will recieve your raised donation through your PAYPAL or BANK ACCOUNT</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:46:28', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 38, NULL, 'Alexis Mercer', 1, NULL, 'alexis-mercer', '<p>dfdfdf</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 15:00:32', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 38, NULL, 'Devin Hartman', 1, NULL, 'devin-hartman', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto nihil saepe laborum architecto, consequuntur mollitia eius quas at cumque rem, nobis reiciendis doloribus. Blanditiis dolorem temporibus suscipit tempora nostrum saepe.</p><div><br></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 16:00:07', 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 18:01:10'),
(63, 38, NULL, 'Kimberly Nunez', 1, NULL, 'kimberly-nunez', '<p>cc</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 13:35:47', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 38, NULL, 'Cain Ball', 1, NULL, 'cain-ball', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 13:37:13', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 38, NULL, 'Kadeem Rios', 3, NULL, 'kadeem-rios', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 13:37:39', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 38, NULL, 'Brenna Barron', 3, NULL, 'brenna-barron', '<p>dfdfd</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 13:39:02', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 39, NULL, 'Gary C.K. Huang', 1, 'MESSAGE FROM ROTARY INTERNATIONAL PRESIDENT', 'gary_c.k._huang', '<p>Rotary is an organization that has something for everyone. All over the world, in cities and towns, for well over a century, Rotarians have come together to serve. And in the years since Rotary was founded, countless Rotarians and others have discovered the joys of Rotary service, through Rotaract, Interact, Rotary Community Corps, Youth Exchange, and many other programs.</p>', '1.jpg', 'RI President', '2014-15', NULL, NULL, NULL, NULL, '2015-07-26 13:39:31', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, NULL, NULL, 'test', 1, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-16 17:21:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, NULL, 1, 't2', 1, NULL, 't2', 'c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-16 17:46:11', NULL, NULL, NULL, NULL, NULL, 'google.com', '', NULL),
(70, NULL, 1, 't3', 0, NULL, 't3', 'ce', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-16 17:47:01', NULL, NULL, NULL, NULL, NULL, 'google.come', '', NULL),
(72, NULL, 0, 'ramesh', 0, NULL, 'ram', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-16 18:40:05', NULL, NULL, NULL, NULL, NULL, '', '', '0000-00-00 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_articles`
--
ALTER TABLE `tbl_articles`
  ADD CONSTRAINT `FK_tbl_articles_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
