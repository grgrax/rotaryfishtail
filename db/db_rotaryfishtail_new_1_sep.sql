-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_rotaryfishtail_new
CREATE DATABASE IF NOT EXISTS `db_rotaryfishtail_new` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_rotaryfishtail_new`;


-- Dumping structure for table db_rotaryfishtail_new.tbl_menus
CREATE TABLE IF NOT EXISTS `tbl_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `page_type_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `article_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) DEFAULT '0',
  `order` int(10) unsigned DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `sidebar` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_menus_ibfk_1` (`parent_id`),
  KEY `tbl_menus_ibfk_2` (`author`),
  KEY `tbl_menus_ibfk_3` (`modified_by`),
  KEY `tbl_menus_ibfk_4` (`page_type_id`),
  KEY `tbl_menus_ibfk_5` (`category_id`),
  KEY `FK_tbl_menus_tbl_articles` (`article_id`),
  CONSTRAINT `FK_tbl_menus_tbl_articles` FOREIGN KEY (`article_id`) REFERENCES `tbl_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_menus` FOREIGN KEY (`parent_id`) REFERENCES `tbl_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_page_types` FOREIGN KEY (`page_type_id`) REFERENCES `tbl_page_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_menus: ~33 rows (approximately)
/*!40000 ALTER TABLE `tbl_menus` DISABLE KEYS */;
INSERT INTO `tbl_menus` (`id`, `parent_id`, `page_type_id`, `category_id`, `article_id`, `name`, `level`, `order`, `slug`, `desc`, `status`, `author`, `modified_by`, `created_at`, `updated_at`, `sidebar`) VALUES
	(1, NULL, 1, NULL, 117, 'Home', 0, 1, 'home', 'home desc', 2, 25, NULL, '2015-03-02 02:24:54', NULL, 'N'),
	(10, 15, 2, NULL, NULL, 'page dfd', 0, 9, 'page-1', '', 3, 25, NULL, '2015-03-03 02:36:03', NULL, NULL),
	(11, 10, 2, 4, NULL, 'page one', 1, 10, 'page-11', '', 2, 25, NULL, '2015-03-03 02:36:24', NULL, NULL),
	(13, 10, 2, 4, NULL, 'page one two', 1, 13, 'page-12', '', 2, 25, NULL, '2015-03-03 02:36:54', NULL, NULL),
	(14, 11, 2, NULL, NULL, 'page 1.1.1', 2, 11, 'page-111', '', 2, 25, NULL, '2015-03-03 02:37:16', NULL, NULL),
	(15, NULL, 2, 4, NULL, 'page', 0, 13, 'page-121', '', 2, 25, NULL, '2015-03-03 02:37:38', NULL, NULL),
	(16, 13, 2, NULL, NULL, 'page 1.2.2', 2, 15, 'page-122', '', 2, 25, NULL, '2015-03-03 02:38:04', NULL, NULL),
	(17, NULL, 1, NULL, NULL, 'n', 0, 1, 'n', 'D', 4, 25, NULL, '2015-03-14 00:54:48', NULL, 'Y'),
	(18, NULL, 1, NULL, 182, 'about us', 0, 2, 'about-us', '', 2, 25, NULL, '2015-03-16 21:32:55', NULL, 'Y'),
	(19, NULL, 2, NULL, NULL, 'Courses', 0, 4, 'courses', '', 2, 25, NULL, '2015-03-16 21:33:11', NULL, NULL),
	(25, NULL, 1, NULL, 64, 'Admission', 0, 14, 'admission', '', 2, 25, NULL, '2015-03-16 21:37:14', NULL, NULL),
	(26, NULL, 1, 39, 31, 'Services', 0, 10, 'services', '', 2, 25, NULL, '2015-03-16 21:37:27', NULL, NULL),
	(27, NULL, 4, 41, NULL, 'news & events', 0, 11, 'news-events', '', 2, 25, NULL, '2015-03-16 21:37:41', NULL, NULL),
	(28, 14, 1, NULL, NULL, 'page 1.1.1.1', 3, 12, 'page-1111', '', 2, 25, NULL, '2015-03-16 22:01:34', NULL, NULL),
	(29, 19, 2, 38, NULL, 'Diploma Level Courses', 1, 5, 'diploma-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:19', NULL, NULL),
	(31, 19, 2, 40, NULL, 'Advanced Level Courses', 1, 6, 'advanced-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:51', NULL, NULL),
	(32, 19, 1, 43, NULL, 'Courses 3', 1, 16, 'courses-3', '', 1, 25, NULL, '2015-03-20 00:18:53', NULL, NULL),
	(33, NULL, 3, 40, NULL, 'Gallery', 0, 12, 'gallery', '', 2, 25, NULL, '2015-03-20 00:45:09', NULL, NULL),
	(35, NULL, 1, NULL, 182, 'Downloads', 0, 16, 'downloads', '', 2, 25, NULL, '2015-03-20 00:48:57', NULL, NULL),
	(36, 25, 6, NULL, 9, 'Contact', 1, 15, 'contact', '', 2, 25, NULL, '2015-03-20 00:58:08', NULL, NULL),
	(37, 39, 1, 34, 46, 'new menu', 2, 9, 'new-menu', 'Voluptatum tempore, pariatur? Illum, nihil numquam soluta similique rerum aut aut dignissimos enim at aliquam et consectetur sed iste harum.', 2, 25, NULL, '2015-03-28 13:10:50', NULL, NULL),
	(38, 19, 2, 39, NULL, 'Chip Level Courses', 1, 7, 'chip-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:06', NULL, NULL),
	(39, 19, 1, 39, NULL, 'Cretification Level Courses', 1, 8, 'cretification-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:20', NULL, NULL),
	(40, 40, 7, 41, 182, 'nabout chals', 0, 15, 'about-chals', 'm', 2, 25, NULL, '2015-05-24 20:31:10', NULL, NULL),
	(41, 46, 7, 40, 166, 'Laura Carson', 1, 18, 'laura-carson', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', 2, 25, NULL, '2015-08-30 23:05:53', NULL, NULL),
	(42, NULL, 7, 40, 166, 'Laura Carsone', 0, 25, 'laura-carsone', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', NULL, 25, NULL, '2015-08-30 23:16:25', NULL, NULL),
	(43, 18, 6, 38, 64, 'Diana Brock', 1, 25, 'diana-brock', 'Et itaque saepe enim fugiat, repudiandae eum laboris sit sint, quod odit atque sint.', NULL, 25, NULL, '2015-08-30 23:19:56', NULL, NULL),
	(44, NULL, 3, 38, 64, 'Emma Acevedo', 2, 18, 'emma-acevedo', 'Soluta odit qui porro dolorem aut duis consequatur? Accusamus minim aut ex fugit, et tempore, aut voluptate aute eligendi voluptas.', 4, 25, NULL, '2015-08-30 23:20:26', NULL, NULL),
	(45, 46, 4, 41, 166, 'Iliana Roberts', 1, 19, 'iliana-roberts', 'Sit molestias maxime culpa, quo veritatis numquam et reiciendis voluptatem irure.', 2, 25, NULL, '2015-08-30 23:21:00', NULL, NULL),
	(46, NULL, 2, 4, NULL, 'new pages', 0, 17, 'new-pages', '', 2, 25, NULL, '2015-08-30 23:51:47', NULL, NULL),
	(47, 18, 1, 41, 51, 'Colton Harmon', 1, 3, 'colton-harmon', 'Ut fugiat, dolor eligendi iste ullamco voluptas beatae laboris pariatur? Repellendus. Non consequatur quos doloribus.', 2, 25, NULL, '2015-08-31 01:02:37', NULL, 'N'),
	(48, 46, 7, 39, 164, 'Zephr Simpson', 1, 20, 'zephr-simpson', 'Consequatur a voluptas ea doloribus voluptas soluta consequatur, inventore veniam, sit, qui nulla blanditiis voluptate sapiente magni.', 2, 25, NULL, '2015-08-31 01:03:10', NULL, 'N'),
	(49, NULL, 2, 39, 161, 'Ingrid Mann', 0, 21, 'ingrid-mann', 'Iste perspiciatis, perferendis officia sint, sit, consequatur? Exercitation dolorum adipisicing dolore et exercitationem esse, nobis aliquam consequatur facilis.', 2, 25, NULL, '2015-08-31 01:09:15', NULL, 'N');
/*!40000 ALTER TABLE `tbl_menus` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
