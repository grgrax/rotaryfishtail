<!DOCTYPE html>
<html>
<head>
    <title>Editor</title>
<script src="http://localhost/editor/js/jquery.js"></script>
<script src="http://localhost/editor/tinymce/tinymce.min.js"></script>
<!-- codemirror theme file -->
<link rel="stylesheet" href="http://localhost/editor/codemirror/theme/abcdef.css">

  <link rel="stylesheet" href="http://localhost/editor/codemirror/doc/docs.css">
  <link rel="stylesheet" href="http://localhost/editor/codemirror/lib/codemirror.css">
  <link rel="stylesheet" href="http://localhost/editor/codemirror/addon/hint/show-hint.css">
  <script src="http://localhost/editor/codemirror/lib/codemirror.js"></script>
  <script src="http://localhost/editor/codemirror/addon/hint/show-hint.js"></script>
  <script src="http://localhost/editor/codemirror/addon/hint/xml-hint.js"></script>
  <script src="http://localhost/editor/codemirror/addon/hint/html-hint.js"></script>
  <script src="http://localhost/editor/codemirror/mode/xml/xml.js"></script>
  <script src="http://localhost/editor/codemirror/mode/javascript/javascript.js"></script>
  <script src="http://localhost/editor/codemirror/mode/css/css.js"></script>
  <script src="http://localhost/editor/codemirror/mode/htmlmixed/htmlmixed.js"></script>

<style type="text/css">
    .CodeMirror {border-top: 1px solid #888; border-bottom: 1px solid #888;}
  </style>
<script type="text/javascript">
        tinymce.init({
            selector: "#mytextarea",
            fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 30pt 36pt 40pt",
            theme: "modern",
            // width: 300,
            height: 400,
            menubar: true,
            browser_spellcheck : true,
          codemirror: {
              path: '../../../codemirror',
              indentOnInit: true,
              extraKeys: {
                'Ctrl-Space': 'autocomplete'
              },
              config: {
                lineNumbers: true,
                // theme: "abcdef",
              }
          },
           image_advtab: true,
           content_css: "css/tinymce.css",
           plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars code codemirror fullscreen insertdatetime media nonbreaking",
                 "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
           ],
           toolbar: "fullscreen | undo redo | fullpage | styleselect fontselect fontsizeselect | forecolor backcolor emoticons | bold italic underline | strikethrough superscript subscript | fontsize | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code preview print", 
          //link unlink responsivefilemanager image media
           external_filemanager_path:"/editor/filemanager/",
           filemanager_title:"Filemanager" ,
           external_plugins: { "filemanager" : "/editor/filemanager/plugin.min.js"},

           style_formats: [
                {title: 'Heading 1', block: 'h1'},
                {title: 'Heading 2', block: 'h2'},
                {title: 'Heading 3', block: 'h3'},
                {title: 'Heading 4', block: 'h4'},
                {title: 'Heading 5', block: 'h5'},
                {title: 'Heading 6', block: 'h6'},
                {title: 'Blockquote', block: 'blockquote', styles: {color: '#333'}},
                {title: 'Pre Formatted', block: 'pre'},
                {title: 'code', block: 'pre', classes: 'code'},
                // {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
        });

    window.onload = function() {
        editor = CodeMirror(document.getElementById("code"), {
          mode: "text/html",
          extraKeys: {"Ctrl-Space": "autocomplete"},
          theme: "abcdef",
          lineNumbers: true,
          // value: document.body.innerHTML
          value: document.documentElement.innerHTML
        });
      };
    </script>
</head>
<body>
    <textarea id="mytextarea"></textarea><div id="code"></div>
</body>
</html>