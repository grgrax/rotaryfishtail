<div id="body">
	<div id="content">
		
		<div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">			
			
			<span>				
				
				<h2><?php echo isset($category['content'])?$category['content']:''?></h2>
				<div id="tabs" style="float:left; width:100%" class="ui-tabs ui-widget ui-widget-content ui-corner-all">

					<!-- tab heading -->
					<?php 
					if(isset($articles) && is_array($articles) && count($articles)>0){  ?>
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
						<?php for ($i=0; $i < count($articles); $i++) { ?>
						<li class="ui-state-default ui-corner-top">
							<a href="#tabs-<?php echo $i+1?>"><?php echo $articles[$i]['name']?></a>
						</li>
						<?php } ?>
					</ul>
					<?php }
					?>
					<!-- tab heading -->

					<!-- tab body -->
					<?php 
					if(isset($articles) && is_array($articles) && count($articles)>0){ 
						for ($i=0; $i < count($articles); $i++) { ?>
						<div id="tabs-<?php echo $i+1?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
							<h3><?php echo $articles[$i]['name']?></h3>
							<br>
							<div>
								<div style="float:left;">
									<div class="photo">
										<?php if($articles[$i]['image'] && is_article_picture_exists($articles[$i]['image'] )){?>
										<img class="course" src="<?php echo is_article_picture_exists($articles[$i]['image'])?>" 
										width="220px" height="180"/>
										<?php } else {?>
										<img class="course" src="<?php echo base_url('templates/assets/media/images/no_image_found.jpg')?>" 
										width="220px" height="180"/>
										<?php } ?>
										<div>
											<div style="text-align:center;width: 230px;">												
												<?php 
												$parts=explode(',', $articles[$i]['image_title']);
												foreach ($parts as $part) {
													echo "<br/>".$part;
												}
												?>											
											</div>
										</div>
										<br>
									</div>
									<div>
										<?php echo $articles[$i]['title']?>
									</div>
								</div>
								<div style="margin-left:30%;">
									<br>
									<?php echo $articles[$i]['content']?>
								</div>
							</div>        
						</div>
						<?php }
					}
					?>
					<!-- tab body -->
				</div>
			</span>
		</div>

		<?php if($menu['sidebar']=='Y'){?>
		<?php $this->load->view('front/includes/sidebar.php') ?>
		<?php } ?>

	</div>
</div>

<style>
	.course img{
		width: 220px !important;
	}
</style>

