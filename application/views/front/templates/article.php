<div id="body">
	<div id="content">
		
		<div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">			
			<span>				
				<h1 id="title"><?php echo isset($article['name'])?$article['name']:''?></h1>
				<?php echo isset($article['content'])?$article['content']:''?>	
			</span>
		</div>

		<?php 
		if($menu['sidebar']=='Y') {
			$this->load->view('front/includes/sidebar.php');
		}
		?>

	</div>
</div>

