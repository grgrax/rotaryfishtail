<!-- sidebar -->
<div class="sidebar">

  <div class="sidebar1">
    <?php 
    $content1=get_setting_value('sidebar1_content');
    if($content1!='n'){
      echo $content1;
    }else{
      $sidebar1_picture=get_setting_value('sidebar1_picture');
      $image1=is_picture_exists("setting/$sidebar1_picture");
      if($sidebar1_picture && $image1) {
        echo '<img alt="" src="'.$image1.'">';
      }
      else{
        echo '<img src="images/rot-brazil.gif" alt="Image">';
      }
    }
    ?>
  </div>

  <div class="sidebar2">
    <h2>Notice</h2>
    <?php 
    $content2=get_setting_value('sidebar2_content');
    if($content2!='n'){
      echo $content2;
    }else{
      $sidebar2_picture=get_setting_value('sidebar2_picture');
      $image2=is_picture_exists("setting/$sidebar2_picture");
      if($sidebar2_picture && $image2) {
        echo '<img alt="" src="'.$image2.'">';
      }
      else{
        echo '<img src="images/rot-brazil.gif" alt="Image">';
      }
    }
    ?>

  </div>
</div>
<!-- sidebar -->


<style>
  .sidebar{
    float: right;
    margin: 0;
    padding: 0;
    width: 295px;
    position: static;
  }
  .sidebar div{
    margin-bottom: 2%;
  }
  .sidebar2 {
    color: #eee;
    font-size: 18px;
    text-align:center;
    background: #085180;
    border: 1px solid #00c6ff;
    height: auto;
    margin: 0;
    padding: 20px 20px 20px;
    width: auto;
  }
  .sidebar2 h2{
    color: #eee !Important;
  }
</style>

<?php /*
<style>
  .sidebar{
    float: right;
    margin: 0;
    padding: 0;
    width: 295px;
    position: static;
  }
  .sidebar div{
    margin-bottom: 2%;
  }
  .sidebar1,.sidebar2 {
    color: #eee !Important;
    font-size: 18px !Important;
    text-align:center !Important;
    background: #085180 !Important;
    border: 1px solid #00c6ff !Important;
    height: auto !Important;
    padding: 20px 20px 20px !Important;
    width: 300px !Important;
    margin-bottom: 2% !Important;
  }
  .sidebar2 h2{
    color: #eee !Important;
  }
</style>
*/ ?>