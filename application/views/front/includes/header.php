<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo get_setting_value('site_name')?></title>

    <link rel="stylesheet" href="<?php echo base_url('templates/front/css/style.css')?>">

    <script type="text/javascript" src="<?php echo base_url('templates/front/scripts/jquery-1.4.1.min.js')?>"></script>


    <link rel="stylesheet" href="<?php echo base_url('templates/front/css/layout.css')?>" />
    <link rel="stylesheet" href="<?php echo base_url('templates/front/lava_menu/lavalamp3.css')?>" />
    <link rel="stylesheet" href="<?php echo base_url('templates/front/css/MenuMatic.css')?>" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="<?php echo base_url('templates/front/css/news_style.css')?>" media="screen"/>

    <!--message tab-->
    <link href="<?php echo base_url('templates/front/css/ui-lightness/jquery-ui-1.8.17.custom.css')?>" rel="stylesheet" />   
    <script type="text/javascript" src="<?php echo base_url('templates/front/js/jquery-ui-1.8.17.custom.min.js')?>"></script>
    <script type="text/javascript">
        $(function(){
            $("#accordion").accordion({ header: "h3" });
            $('#tabs').tabs();

            $('#slider').slider({
                range: true,
                values: [17, 67]
            });
        });
    </script>


</head>
<body>

    <!--lavalamp menu-->
    <script>
        <script type="text/javascript" src="<?php echo base_url('templates/front/lava_menu/lib/jquery.min.js')?>"></script>
    </script>
    <script type="text/javascript" src="<?php echo base_url('templates/front/lava_menu/lib/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('templates/front/lava_menu/lib/jquery.lavalamp.1.3.3-min.js')?>"></script>

    <!-- slider  -->
    <script type="text/javascript">
        var query = new Object();
        window.location.search.replace(
            new RegExp( "([^?=&]+)(=([^&]*))?", 'g' ),
            function( $0, $1, $2, $3 ){
                query[ $1 ] = $3;
            }
            );
        easing = query['e'] || 'Expo';

        function loadEasing(e) {
            location.href = location.pathname+'?e='+e;
        }

        function setEasing(e) {
            loadLamps(e);
        }

        function loadLamps(easing) {
            $('#lavaLampBasicImage').lavaLamp({
                fx: 'easeIn'+easing,
                speed: 800,
                homeTop:-1,
                homeLeft:-1
            });

            $('#lavaLampVariableImage').lavaLamp({
                fx: 'easeOut'+easing,
                speed: 800,
                startItem:1
            });

            $('#lavaLampBorderOnly').lavaLamp({
                fx: 'easeInOut'+easing,
                speed: 1000,
                returnDelay:1000
            });

            $('#lavaLampVertical').lavaLamp({
                fx: 'easeOut'+easing,
                speed: 1000,
                click: function() {return false;},
                setOnClick: false
            });

            $('#lavaLampFun').lavaLamp({
                fx: 'easeInOut'+easing,
                speed: 1000,
                autoReturn: false
            });

            $('#lavaLampHome').lavaLamp({
                fx: 'easeInOut'+easing,
                speed: 1000,
                returnHome: true,
                homeTop: -25,
                homeWidth:300,
                homeHeight:10
            });

            $('#lavaLampMulti').lavaLamp({
                speed: 500
            });

        }

        $(function() {
            $('#menu').lavaLamp({fx: 'swing', speed: 333});
            loadLamps(easing);

            $('select#easing option[value='+easing+']').attr('selected','selected');
            $('.easingLabel').text(easing);
        });
    </script>
    <!-- slider  -->







    <div id="header">
        <div>
            <br>

            <div class="first">
                <font face="Geneva, sans-serif" size=6 color="#006699"><span>
                    <a href="<?php echo base_url();?>">
                        <?php
                        $header_logo=get_setting_value('header_logo');
                        if($header_logo){ ?>
                        <img src="<?php echo base_url("uploads/files/pics/setting/$header_logo")?>" width="100" height="101">
                        <?php } else{ ?>
                        <img src="<?php echo base_url('templates/front/images/rcp.png')?>">
                        <?php }
                        ?>
                        <span>
                            <?php echo get_setting_value('club_name') ?>
                        </span>
                    </a></span>
                </font>
                <span class="bardown">
                    <h2>
                        <?php echo get_setting_value('club_number') ?>
                    </h2>
                </span>
            </div>

            <div class="last">

                <ul id="lavaLampHome" class="lamp">
                    <li>
                        <a href="<?php echo get_setting_value('other_site_1_url') ?>" target="_blank" >
                            <p style="color:#085180; font-size:15px"><?php echo get_setting_value('other_site_1_name') ?></p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_setting_value('other_site_2_url') ?>" target="_blank" >
                            <p style="color:#085180; font-size:15px"><?php echo get_setting_value('other_site_2_name') ?></p>
                        </a>
                    </li>
                </ul>
                <h3> 
                    <!-- "Service above Self" -->
                    <?php echo get_setting_value('slogan_head') ?>
                </h3>
                <h2> 
                    <!-- ROTARY INTERNATIONAL -->
                    <?php echo get_setting_value('slogan_body') ?>
                </h2>
                <p> 
                    <!-- Dist. 3292 -->
                    <?php echo get_setting_value('slogan_dist') ?>
                </p>
            </div>

            <!-- BEGIN Menu -->
            <?php 
            $data_menu=get_menus(); 
            echo get_ol($data_menu);
            ?>
        </div>

        <?php 


        function get_ol($array, $child = FALSE,$level=0)
        {
            $str = '';   
            if (count($array)) {
                $str .= $child == TRUE ? PHP_EOL.'<ul>': PHP_EOL. '<ul id="nav">' ;
                foreach ($array as $item) {
                    $childs=no_of_child_menus($item['id']);
                    if(isset($item['children'])){
                        if($item['parent_id']){
                            $str .= PHP_EOL.'<li>';
                            $str .='<a href="'.base_url($item['url']).'">'.$item['name'].'</a>';
                        }                
                        else{
                            $str .= PHP_EOL.'<li>';
                            $str .='<a href="#"">'.$item['name'].'</a>';
                        }
                    }
                    else{
                        $str .= PHP_EOL.'<li>';
                        $str .='<a href="'.base_url($item['url']).'">'.$item['name'].'</a></li>';
                    }
                        // Do we have any children?
                    if (isset($item['children']) && count($item['children'])) {
                        $str .= get_ol($item['children'], TRUE);
                    }
                    $str .= '</li>' . PHP_EOL;
                }
                $str .= '</ul>' . PHP_EOL;
            }
            return $str;
        }
        ?>

    </div>

