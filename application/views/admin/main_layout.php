 <?php 

 $admin_template=get_admin_template(config_item('admin_template'));
 $path="templates/".$admin_template['path'];
 
 require_once($path."/includes/header.php"); 
 
 $CI =& get_instance();
 if($CI->session->flashdata('success')){
 	$class="alert-success";
 	$message=$CI->session->flashdata('success');
 }
 else if($CI->session->flashdata('error')){
 	$class="alert-danger";
 	$message=$CI->session->flashdata('error');
 }
 ?>


 <!--Page content-->
 <!--===================================================-->
 <div id="page-content">
 	<?php 
 	template_validation();
 	if(isset($class) && isset($message)){ 
 		flash_msg($class,$message);
 	}
 	$this->load->view($subview);
 	?>
 </div>
 <!--===================================================-->
 <!--End page content-->
 <?php
 require_once($path."/includes/footer.php"); 
 ?>

