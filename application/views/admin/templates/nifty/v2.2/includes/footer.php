</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
  <div id="mainnav">


    <!--Menu-->
    <!--================================-->
    <div id="mainnav-menu-wrap">
      <div class="nano">
        <div class="nano-content">
          <ul id="mainnav-menu" class="list-group">





            <br>
            <br>

            <!--Category name-->
            <li class="list-header">Manage</li>


            <!-- menu -->
            <li>
              <a href="#">
                <i class="fa fa-navicon"></i>
                <span class="menu-title">Menu
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('menu')?>">List</a></li>
                <li> <a href="<?=base_url('menu/add')?>">Add</a></li>
                <li> <a href="<?=base_url('menu/order')?>">Order</a></li>
              </ul>
            </li>
            <!-- menu -->

            <!-- category -->
            <li>
              <a href="#">
                <i class="fa fa-list-ul"></i>
                <span class="menu-title">Category
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('category')?>">List</a></li>
                <li> <a href="<?=base_url('category/add')?>">Add</a></li>
              </ul>
            </li>
            <!-- category -->


            <!-- article -->
            <li>
              <a href="#">
                <i class="fa fa-newspaper-o"></i>
                <span class="menu-title">Article
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('article')?>">List</a></li>
                <li> <a href="<?=base_url('article/add')?>">Add</a></li>
              </ul>
            </li>
            <!-- article -->


            <li class="list-divider"></li>
            <!--Category name-->
            <li class="list-header">User & Groups</li>


            <!-- group -->
            <li>
              <a href="javascritp:;">
                <i class="fa fa-users"></i>
                <span class="menu-title">Groups
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('group')?>">List</a></li>
                <li> <a href="<?=base_url('group/add')?>"/>Add</a></li>
              </ul>
            </li>
            <!-- group -->

            <!-- user -->
            <li>
              <a href="javascritp:;">
                <i class="fa fa-user"></i>
                <span class="menu-title">Users
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('user')?>">List</a></li>
                <li> <a href="<?=base_url('user/add')?>"/>Add</a></li>
              </ul>
            </li>
            <!-- user -->

            <!--Category name-->
            <li class="list-header">Create Contents</li>


            <!-- group -->
            <li>
              <a href="javascritp:;">
                <i class="fa fa-plus-square"></i>
                <span class="menu-title">Add
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('menu/add')?>">Menu</a></li>
                <li> <a href="<?=base_url('category/add')?>"/>Category</a></li>
                <li> <a href="<?=base_url('article/add')?>"/>Article</a></li>
              </ul>
            </li>
            <!-- group -->
            <li> 
              <a href="<?=base_url('plugin/filemanager')?>"/>
                <i class="fa fa-file-image-o "></i>
                Files ( Pictures )
              </a>
            </li>

          </ul>


        </div>
      </div>
    </div>
    <!--================================-->
    <!--End menu-->

  </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->


</div>



<!-- FOOTER -->
<!--===================================================-->
<footer id="footer">

  <!-- Visible when footer positions are fixed -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <div class="pull-right">
    <ul class="footer-list list-inline">
      <!-- <li>
        <p class="text-sm">SEO Proggres</p>
        <div class="progress progress-sm progress-light-base">
          <div style="width: 80%" class="progress-bar progress-bar-danger"></div>
        </div>
      </li>

      <li>
        <p class="text-sm">Online Tutorial</p>
        <div class="progress progress-sm progress-light-base">
          <div style="width: 80%" class="progress-bar progress-bar-primary"></div>
        </div>
      </li>
      <li>
        <button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
      </li> -->
    </ul>
  </div>



  <!-- Visible when footer positions are static -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <div class="hide-fixed pull-right pad-rgt">&#0169; 2015 <?=config_item('developer')?></div>



  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

  <!-- <p class="pad-lft">&#0169; 2015 <?=config_item('developer')?></p> -->



</footer>
<!--===================================================-->
<!-- END FOOTER -->


<!-- SCROLL TOP BUTTON -->
<!--===================================================-->
<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
<!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


<!--JAVASCRIPT-->
<!--=================================================-->



<!--BootstrapJS [ RECOMMENDED ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/bootstrap.min.js"></script> -->


<!--Fast Click [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/fast-click/fastclick.min.js"></script>


<!--Nifty Admin [ RECOMMENDED ]-->
<script src="<?=admin_template_asset_path()?>/js/nifty.min.js"></script>


<!--Morris.js [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/morris-js/morris.min.js"></script> -->
<script src="<?=admin_template_asset_path()?>/plugins/morris-js/raphael-js/raphael.min.js"></script>


<!--Sparkline [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/sparkline/jquery.sparkline.min.js"></script>


<!--Skycons [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/skycons/skycons.min.js"></script>


<!--Switchery [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/switchery/switchery.min.js"></script>


<!--Bootstrap Select [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/bootstrap-select/bootstrap-select.min.js"></script>

<!-- template validation -->
<!--Bootstrap Validator [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/bootstrap-validator/bootstrapValidator.min.js"></script> -->
<!--Masked Input [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/masked-input/jquery.maskedinput.min.js"></script> -->
<!-- template validation -->

<!--Demo script [ DEMONSTRATION ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/nifty-demo.min.js"></script> -->


<!--Specify page [ SAMPLE ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/dashboard.js"></script> -->

<!--Panel [ SAMPLE ]-->
<script src="<?=admin_template_asset_path()?>/js/demo/ui-panels.js"></script>

<!--Specify page [ SAMPLE ]-->
<script src="<?=admin_template_asset_path()?>/js/custom.js"></script>



<!-- footables -->
<!-- <link href="<?=admin_template_asset_path()?>/plugins/fooTable/css/footable.core.css" rel="stylesheet">
<script src="<?=admin_template_asset_path()?>/plugins/fooTable/dist/footable.all.min.js"></script>
<script src="<?=admin_template_asset_path()?>/js/demo/tables-footable.js"></script>
--><!-- footables -->


<!-- nestable -->
<script src="<?=base_url()?>templates/assets/nestable/jquery-ui-1.9.1.custom.min.js"></script>
<script src="<?=base_url()?>templates/assets/nestable/jquery.mjs.nestedSortable.js"></script>

<!-- nestable -->


<!-- core jquery -->
<link rel="stylesheet" href="<?=base_url()?>/templates/assets/jquery-v1.11.4/jquery-ui.css">
<script src="<?=base_url()?>/templates/assets/jquery-v1.11.4/jquery-ui.js"></script>
<!-- core jquery -->


<!-- bootstrap tinymice v 3 -->
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/wysihtml5x-toolbar.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/core/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/handlebars.runtime.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/bootstrap3-wysihtml5.min.js"></script>
<!-- bootstrap tinymice v 3 -->

<!-- bootsrtap components -->
<script src="<?=base_url()?>/templates/assets/bootstrap/components/bootbox-v4.4.0/bootbox.min.js"></script>
<!-- bootsrtap components -->


<!--Form validation [ SAMPLE ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/form-validation.js"></script> -->
<script src="<?php echo template_path('assets/js/campaign.js')?>" type="text/javascript"></script>

<link rel="stylesheet" href="<?=base_url()?>/templates/assets/nestable/nestable.css">


<script>

  $(function(){

    try{

      tinyMCE.DOM.addClass('table', 'bod');


      $('[data-toggle="tooltip"]').tooltip(); 

      $('<br class="clear">').insertAfter('ul.pagination');


      $('.textarea').wysihtml5();

      var validate='<?php echo $this->config->item("enabe_jq_validation_backend")?>';
      // console.log(validate);
      if(validate==1){
        $("#validate_form").validate();
      }

      $('a.delete').on("click", function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        bootbox.confirm("Are you sure?", function(result) {
          if(result){
            window.location=url;
          }
        });
      });

      tinymce.init({
        selector: "#mytextarea",
        fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 30pt 36pt 40pt",
        theme: "modern",
        height: 400,
        menubar: true,
        browser_spellcheck : true,
        codemirror: {
          path: '../../../codemirror',
          indentOnInit: true,
          extraKeys: {
            'Ctrl-Space': 'autocomplete'
          },
          config: {
            lineNumbers: true,
          }
        },
        body_id: "my_id",
        image_advtab: true,
        content_css: "<?php echo base_url('templates/front/css/style.css')?>, <?php echo base_url('templates/front/css/layout.css')?>",
        plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code codemirror fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
        ],
        toolbar: "fullscreen | undo redo | fullpage | styleselect | removeformat | fontselect fontsizeselect | forecolor backcolor emoticons | bold italic underline | strikethrough superscript subscript | fontsize | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code preview print | link image | template", 
        templates : [
        {
          title: "Club Member's Information",
          url: "<?php echo base_url('article/club_member/add')?>",
          description: "Club Member's Information description"
        }],
        external_filemanager_path:"<?php echo base_url().'plugin/filemanager/'?>",
        filemanager_title:"Filemanager" ,
        external_plugins: { "filemanager" : "<?php echo base_url().'templates/assets/editor/filemanager/plugin.min.js'?>" },
        style_formats: [
        {title: 'Heading 1', block: 'h1'},
        {title: 'Heading 2', block: 'h2'},
        {title: 'Heading 3', block: 'h3'},
        {title: 'Heading 4', block: 'h4'},
        {title: 'Heading 5', block: 'h5'},
        {title: 'Heading 6', block: 'h6'},
        {title: 'Blockquote', block: 'blockquote', styles: {color: '#333'}},
        {title: 'Pre Formatted', block: 'pre'},
        {title: 'code', block: 'pre', classes: 'code'},
        ],
        relative_urls: false,
        remove_script_host : true
      });



}
catch(e){
  console.log(e.message)
}


});
</script>
<style>
  .clear{
    clear: both;
  }
</style>

<?php //show_pre(get_session('logged_in_user')); ?>

</body>

</html>
<?php if(ENVIRONMENT=='development'){
  // show_pre($this->session->userdata);
}
?>


